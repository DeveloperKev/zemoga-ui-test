import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Layout",
    component: () =>
    import(/* webpackChunkName: "about" */ "../layout/LayoutPage.vue")
  },
  {
  path: "/layout",
  name: "Layout",
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  component: () =>
    import(/* webpackChunkName: "about" */ "../layout/LayoutPage.vue")
  },
  {
    path: "/i18n",
    name: "I18n",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../components/HelloI18n.vue")
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

export default router;
